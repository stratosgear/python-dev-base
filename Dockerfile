FROM python:3.7.4

LABEL maintainer="Stratos Gerakakis <stratosgear@gmail.com>"

# Install system tools
# RUN apt-get update && apt-get install -y \
#    git 

# Install python tools
RUN pip install poetry reno semver